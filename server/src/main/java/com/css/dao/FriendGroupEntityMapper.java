package com.css.dao;

import com.css.entity.FriendGroupEntity;

public interface FriendGroupEntityMapper {
    int deleteByPrimaryKey(Long id);

    int insert(FriendGroupEntity record);

    int insertSelective(FriendGroupEntity record);

    FriendGroupEntity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(FriendGroupEntity record);

    int updateByPrimaryKey(FriendGroupEntity record);
}