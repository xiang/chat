package com.css.service;

import com.css.config.base.Constants;
import com.css.config.base.SpringContext;
import com.css.config.netty.IoSession;
import com.css.config.netty.enums.SessionManager;
import com.css.logic.login.event.UserLoginEvent;
import com.css.logic.login.message.res.ResUserLogin;
import com.css.util.listenter.EventType;
import com.css.util.model.User;
import com.css.util.netty.ChannelUtils;
import io.netty.channel.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LoginService {

	@Autowired
	private UserService userService;

	public void validateLogin(Channel channel, long userId, String password) {
		User user = userService.queryUser(userId, password);
		IoSession session = ChannelUtils.getSessionBy(channel);
		if (user == null) {
			SessionManager.INSTANCE.sendPacketTo(session,
					ResUserLogin.valueOfFailed());
			return;
		}

		onLoginSucc(user, session);
	}

	private void onLoginSucc(User user, IoSession session) {
		SessionManager.INSTANCE.registerSession(user, session);
		userService.addUser2Online(user);

		ResUserLogin loginPact = new ResUserLogin();
		loginPact.setIsValid(Constants.TRUE);
		SessionManager.INSTANCE.sendPacketTo(session, loginPact);

		SpringContext.getEventDispatcher().fireEvent(
				new UserLoginEvent(EventType.LOGIN, user.getUserId()));
	}

}
