package com.css.util.listenter;

public enum EventType {

	/** 登录事件  */
	LOGIN,

	/** 登出事件  */
	LOGOUT,

	/** 升级事件 */
	LEVEL_UP;

}
