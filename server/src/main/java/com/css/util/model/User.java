package com.css.util.model;

public class User {

	private long userId;
	/** 性别*/
	private byte sex;
	/** 用户名字 */
	private String userName;
	/** 密码 */
	private String password;
	/** 个性签名　*/
	private String signature;

	public User(){}

	public User(com.css.entity.User users){
		this.userId = users.getUserid();
		this.password = users.getPassword();
		this.userName = users.getUsername();
		this.signature = users.getSignature();
	}

	public long getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public byte getSex() {
		return sex;
	}
	public void setSex(byte sex) {
		this.sex = sex;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}

}
