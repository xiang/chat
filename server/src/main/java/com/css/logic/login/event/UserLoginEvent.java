package com.css.logic.login.event;


import com.css.util.listenter.EventType;
import com.css.util.listenter.UserEvent;

public class UserLoginEvent extends UserEvent {

	public UserLoginEvent(EventType evtType, long userId) {
		super(evtType, userId);
	}

}
