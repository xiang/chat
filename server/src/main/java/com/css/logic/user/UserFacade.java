package com.css.logic.user;

import com.css.config.base.SpringContext;
import com.css.logic.login.event.UserLoginEvent;
import com.css.service.UserService;
import com.css.util.listenter.EventType;
import com.css.util.listenter.annotation.EventHandler;
import com.css.util.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserFacade {

	@Autowired
	private UserService userService;

	@EventHandler(value = { EventType.LOGIN })
	public void onUserLogin(UserLoginEvent loginEvent) {
		long userId = loginEvent.getUserId();
		User user = SpringContext.getUserService().getOnlineUser(userId);
		userService.refreshUserProfile(user);
	}

}
