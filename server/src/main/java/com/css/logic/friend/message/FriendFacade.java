package com.css.logic.friend.message;

import com.css.config.base.SpringContext;
import com.css.logic.login.event.UserLoginEvent;
import com.css.service.FriendService;
import com.css.service.UserService;
import com.css.util.listenter.EventType;
import com.css.util.listenter.annotation.EventHandler;
import com.css.util.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FriendFacade {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private FriendService friendService;

	@EventHandler(value = { EventType.LOGIN })
	public void onUserLogin(UserLoginEvent loginEvent) {
		long userId = loginEvent.getUserId();
		logger.info("用户登陆{}",userId);
		User user = SpringContext.getUserService().getOnlineUser(userId);
		friendService.refreshUserFriends(user);
	}

}
