package com.css.logic.chat.message.req;

import com.css.config.base.SpringContext;
import com.css.config.netty.IoSession;
import com.css.config.netty.enums.PacketType;
import com.css.config.netty.message.AbstractPacket;
import com.css.config.netty.message.FileUpload;
import io.netty.buffer.ByteBuf;

/**
 * @author zyj
 * @description
 * @date 2019/8/1 15:36
 **/
public class ImgChatToUser extends AbstractPacket {

    private long toUserId;
    private FileUpload fileUpload;


    public FileUpload getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(FileUpload fileUpload) {
        this.fileUpload = fileUpload;
    }

    public long getToUserId() {
        return toUserId;
    }

    public void setToUserId(long toUserId) {
        this.toUserId = toUserId;
    }



    @Override
    public void writeBody(ByteBuf buf) {
        buf.writeLong(this.toUserId);
        writeFile(buf, fileUpload);
    }

    @Override
    public void readBody(ByteBuf buf) {
        this.toUserId = buf.readLong();
        fileUpload = readFile(buf);
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.ReqChatToUser;
    }

    @Override
    public void execPacket(IoSession session) {
        SpringContext.getChatService().sendImg(session, toUserId, fileUpload);
    }

}
