package com.css.config.netty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author zyj
 * @description
 * @date 2019/7/18 17:19
 **/
@Component
public class NettyStartupRunner  implements CommandLineRunner {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void run(String... args) throws Exception {
        ChatServer chatServer = new ChatServer();
        chatServer.init();
        chatServer.start();
    }
}
