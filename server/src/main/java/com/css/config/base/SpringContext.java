package com.css.config.base;

import com.css.config.netty.thread.MessageDispatcher;
import com.css.logic.chat.ChatService;
import com.css.service.FriendService;
import com.css.service.UserService;
import com.css.config.properties.NettyServerConfigs;
import com.css.util.commonutil.ConcurrentHashSet;
import com.css.util.listenter.EventDispatcher;
import com.css.util.model.LruHashMap;
import com.css.util.model.User;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;
import java.util.Set;

/**
 * @author zyj
 * @description
 * @date 2019/7/18 17:35
 **/
@Component
public class SpringContext implements ApplicationContextAware {
    /** spring容器上下文 */
    private static ApplicationContext applicationContext = null;
    private static NettyServerConfigs serverConfigs;
    private static UserService userService;
    private static FriendService friendService;
    private static MessageDispatcher messageDispatcher;
    private static EventDispatcher eventDispatcher;
    private static ChatService chatService;
    /** 在线用户列表　*/
    private Set<Long> onlneUsers = new ConcurrentHashSet<>();
    /** lru缓存最近登录的所有用户 */
    private Map<Long, User> lruUsers = new LruHashMap<>(1000);

    @Resource
    public void setServerConfigs(NettyServerConfigs serverConfigs) {
        SpringContext.serverConfigs = serverConfigs;
    }

    public final static NettyServerConfigs getServerConfigs() {
        return SpringContext.serverConfigs;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringContext.applicationContext = applicationContext;
    }

    @Resource
    public void setUserService(UserService userService) {
        SpringContext.userService = userService;
    }

    public final static UserService getUserService() {
        return userService;
    }

    @Resource
    public void setFriendService(FriendService friendService) {
        SpringContext.friendService = friendService;
    }

    public final static FriendService getFriendService() {
        return friendService;
    }

    @Resource
    public void setMessageDispatcher(MessageDispatcher messageDispatcher) {
        SpringContext.messageDispatcher = messageDispatcher;
    }

    public final static MessageDispatcher getMessageDispatcher() {
        return messageDispatcher;
    }

    public final static <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz);
    }

    @Resource
    public void setEventDispatcher(EventDispatcher eventDispatcher) {
        SpringContext.eventDispatcher = eventDispatcher;
    }

    public final static EventDispatcher getEventDispatcher() {
        return eventDispatcher;
    }

    public boolean isOnlineUser(long userId) {
        return this.onlneUsers.contains(userId);
    }

    public User getOnlineUser(long userId) {
        return this.lruUsers.get(userId);
    }


    @Resource
    public void setChatService(ChatService chatService) {
        SpringContext.chatService = chatService;
    }

    public final static ChatService getChatService() {
        return chatService;
    }


}
