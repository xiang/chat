package com.css.chat.logic.user;

import com.css.chat.logic.user.message.res.ResUserInfo;
import com.css.chat.logic.user.model.UserModel;

/**
 * @author zyj
 * @description
 * @date 2019/7/25 14:39
 **/
public class UserManager {
    private static UserManager instance = new UserManager();

    private UserModel profile = new UserModel();

    public static UserManager getInstance() {
        return instance;
    }

    public void updateMyProfile(ResUserInfo userInfo) {
        profile.setSex(userInfo.getSex());
        profile.setSignature(userInfo.getSignature());
        profile.setUserId(userInfo.getUserId());
        profile.setUserName(userInfo.getUserName());
    }

    public UserModel getMyProfile() {
        return this.profile;
    }

    public long getMyUserId() {
        return this.profile.getUserId();
    }
}
