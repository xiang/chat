package com.css.chat.logic.chat.message.req;

import com.css.chat.logic.chat.SendVideoImg;
import com.css.chat.netty.message.AbstractPacket;
import com.css.chat.netty.message.FileUpload;
import com.css.chat.netty.message.PacketType;
import io.netty.buffer.ByteBuf;

/**
 * @description:
 * @author: zyj
 * @create: 2020-03-04 13:58
 **/
public class VideoToUser extends AbstractPacket {

    private static VideoToUser self = new VideoToUser();

    private VideoToUser() {}

    public static VideoToUser getInstance() {
        return self;
    }

    private long toUserId;
    /**0:关闭 1：打开*/
    private int close;
    private byte[] videoImg;

    public long getToUserId() {
        return toUserId;
    }

    public void setToUserId(long toUserId) {
        this.toUserId = toUserId;
    }

    public int getClose() {
        return close;
    }

    public void setClose(int close) {
        this.close = close;
    }

    public byte[] getVideoImg() {
        return videoImg;
    }

    public void setVideoImg(byte[] videoImg) {
        this.videoImg = videoImg;
    }

    @Override
    public void readBody(ByteBuf buf) {
        this.toUserId = buf.readLong();
        this.close = buf.readInt();
        int length = buf.readInt();
        buf.readBytes(length);
    }

    @Override
    public void writeBody(ByteBuf buf) {
        buf.writeLong(this.toUserId);
        buf.writeInt(close);
        buf.writeInt(videoImg.length);
        buf.writeBytes(videoImg);
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.REQ_VIDEO_DNL_IMG;
    }

    @Override
    public void execPacket() {

    }
}
