package com.css.chat.logic.login.message.req;

import com.css.chat.netty.message.AbstractPacket;
import com.css.chat.netty.message.PacketType;
import io.netty.buffer.ByteBuf;

public class ReqHeartBeat extends AbstractPacket {

	@Override
	public void writeBody(ByteBuf buf) {
	}

	@Override
	public void readBody(ByteBuf buf) {
		
	}

	@Override
	public PacketType getPacketType() {
		return PacketType.ReqHeartBeat;
	}

	@Override
	public void execPacket() {
		System.out.println("收到客户端的心跳回复");
	}

}
