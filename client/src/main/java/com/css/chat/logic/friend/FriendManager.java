package com.css.chat.logic.friend;

import com.css.chat.base.Constants;
import com.css.chat.base.UiBaseService;
import com.css.chat.logic.friend.vo.FriendItemVo;
import com.css.chat.logic.user.UserManager;
import com.css.chat.ui.R;
import com.css.chat.ui.StageController;
import com.css.chat.util.ImageUtil;
import com.css.chat.util.event.DoubleClickEventHandler;
import javafx.event.Event;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.*;

/**
 * @author zyj
 * @description
 * @date 2019/7/25 17:09
 **/
public class FriendManager {

    private static FriendManager instance = new FriendManager();

    private Map<Long, FriendItemVo> friends = new HashMap<>();

    private Map<Integer, String> groupNames = new HashMap<>();
    /** 分组好友视图 */
    private TreeMap<Integer, List<FriendItemVo>> groupFriends = new TreeMap<>();

    public static FriendManager getInstance() {
        return instance;
    }

    /**
     * 刷新朋友列表
     * @author zyj
     * @date 2019/7/25 17:15
     * @param friendItems
     * @return
     */
    public void receiveFriendsList(List<FriendItemVo> friendItems) {
        friends.clear();
        for (FriendItemVo item:friendItems) {
            friends.put(item.getUserId(), item);
        }
        rangeToGroupFriends(friendItems);

        UiBaseService.INSTANCE.runTaskInFxThread(() -> {
            refreshMyFriendsView(friendItems);
        });

    }

    /**
     * 调整成好友分组结构
     */
    private void rangeToGroupFriends(List<FriendItemVo> friendItems) {
        this.groupFriends.clear();
        TreeMap<Integer, List<FriendItemVo>> groupFriends = new TreeMap<>();
        for (FriendItemVo item:friendItems) {
            int groupId= item.getGroup();
            List<FriendItemVo> frendsByGroup = groupFriends.get(groupId);
            if (frendsByGroup == null) {
                frendsByGroup = new ArrayList<>();
                groupFriends.put(groupId, frendsByGroup);
            }
            this.groupNames.put(groupId, item.getGroupName());
            frendsByGroup.add(item);
        }
        this.groupFriends = groupFriends;
    }

    /**
     * 把分好的列表放入Accordion中
     * @author zyj
     * @date 2019/7/25 17:17
     * @param friendItems
     * @return
     */
    public void refreshMyFriendsView(List<FriendItemVo> friendItems) {
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        Stage stage = stageController.getStageBy(R.id.MainView);
        ScrollPane scrollPane = (ScrollPane)stage.getScene().getRoot().lookup("#friendSp");
        Accordion friendGroup = (Accordion)scrollPane.getContent();
        friendGroup.getPanes().clear();

        for (Map.Entry<Integer, List<FriendItemVo>> entry:groupFriends.entrySet()) {
            int groupId = entry.getKey();
            String groupName = this.groupNames.get(groupId);
            decorateFriendGroup(friendGroup, groupName, entry.getValue());
        }
    }

    private void decorateFriendGroup(Accordion container, String groupName, List<FriendItemVo> friendItems) {
        ListView<Node> listView = new ListView<Node>();
        int onlineCount = 0;
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        for (FriendItemVo item:friendItems) {
            if (item.isOnlie()) {
                onlineCount++;
            }
            Pane pane = stageController.load(R.layout.FriendItem, Pane.class);
            decorateFriendItem(pane, item);
            listView.getItems().add(pane);
        }

        bindDoubleClickEvent(listView);
        String groupInfo = groupName + " " + onlineCount+"/"+friendItems.size();
        TitledPane tp = new TitledPane(groupInfo, listView);
        container.getPanes().add(tp);
    }

    /**
     * 画元素Item
     * @author zyj
     * @date 2019/7/25 17:24
     * @param itemUi
     * @param friendVo
     * @return
     */
    private void decorateFriendItem(Pane itemUi, FriendItemVo friendVo) {
        Label autographLabel = (Label) itemUi.lookup("#signature");
        autographLabel.setText(friendVo.getSignature());
        Hyperlink usernameUi = (Hyperlink) itemUi.lookup("#userName");
        usernameUi.setText(friendVo.getFullName());

        //隐藏域，聊天界面用
        Label userIdUi = (Label)itemUi.lookup("#friendId");
        userIdUi.setText(String.valueOf(friendVo.getUserId()));

        ImageView headImage = (ImageView) itemUi.lookup("#headIcon");

        if (!friendVo.isOnlie()) {
            headImage.setImage(ImageUtil.convertToGray(headImage.getImage()));
        }

    }

    /**
     * 鼠标双击事件
     * @author zyj
     * @date 2019/7/25 17:38
     * @param listView
     * @return
     */
    private void bindDoubleClickEvent(ListView<Node> listView) {
        listView.setOnMouseClicked(new DoubleClickEventHandler<Event>() {
            @Override
            public void handle(Event event) {
                if (this.checkVaild()) {
                    ListView<Node> view = (ListView<Node>) event.getSource();
                    Node selectedItem = view.getSelectionModel().getSelectedItem();
                    if (selectedItem == null)
                        return;
                    Pane pane = (Pane) selectedItem;
                    Label userIdUi = (Label)pane.lookup("#friendId");

                    long friendId = Long.parseLong(userIdUi.getText());
                    FriendItemVo targetFriend = friends.get(friendId);

                    long selfId = UserManager.getInstance().getMyUserId();
                    if (friendId == selfId) {
                        //不能跟自己聊天
                        return;
                    }
                    if (targetFriend != null) {
                        openChat2PointPanel(targetFriend);
                    }
                }
            }
        });
    }

    private void openChat2PointPanel(FriendItemVo targetFriend) {
        StageController stageController = UiBaseService.INSTANCE.getStageController();
        Stage chatStage = stageController.setStage(R.id.ChatToPoint);

        Label userIdUi = (Label)chatStage.getScene().getRoot().lookup("#userIdUi");
        userIdUi.setText(String.valueOf(targetFriend.getUserId()));
        Hyperlink userNameUi = (Hyperlink)chatStage.getScene().getRoot().lookup("#userName");
        Label signatureUi = (Label)chatStage.getScene().getRoot().lookup("#signature");
        userNameUi.setText(targetFriend.getFullName());
        signatureUi.setText(targetFriend.getSignature());
    }


    /**
     * 好友登录刷新
     * @param friendId
     */
    public void onFriendLogin(long friendId) {
        FriendItemVo friend = friends.get(friendId);
        if (friend != null) {
            friend.setOnline(Constants.ONLINE_STATUS);
            List<FriendItemVo> friendItems = new ArrayList<>(friends.values());
            receiveFriendsList(friendItems);
        }
    }

    /**
     * 好友下线刷新
     * @param friendId
     */
    public void onFriendLogout(long friendId) {
        FriendItemVo friend = friends.get(friendId);
        if (friend != null) {
            friend.setOnline(Constants.OFFLINE_STATUS);
            List<FriendItemVo> friendItems = new ArrayList<>(friends.values());
            receiveFriendsList(friendItems);
        }
    }
}
