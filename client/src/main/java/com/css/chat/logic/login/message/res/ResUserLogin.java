package com.css.chat.logic.login.message.res;

import com.css.chat.logic.login.LoginManager;
import com.css.chat.netty.message.AbstractPacket;
import com.css.chat.netty.message.PacketType;
import io.netty.buffer.ByteBuf;

/**
 * @author zyj
 * @description
 * @date 2019/7/22 17:17
 **/
public class ResUserLogin extends AbstractPacket{

    private String alertMsg;
    private byte isValid;

    @Override
    public void writeBody(ByteBuf buf) {
        writeUTF8(buf, alertMsg);
        buf.writeByte(isValid);
    }

    @Override
    public void readBody(ByteBuf buf) {
        this.alertMsg = readUTF8(buf);
        this.isValid = buf.readByte();
    }

    @Override
    public PacketType getPacketType() {
        return PacketType.RespUserLogin;
    }

    @Override
    public void execPacket() {
        System.out.println("receive login "+ alertMsg);
        LoginManager.getInstance().handleLoginResponse(this);
    }

    public String getAlertMsg() {
        return alertMsg;
    }

    public void setAlertMsg(String alertMsg) {
        this.alertMsg = alertMsg;
    }

    public byte getIsValid() {
        return isValid;
    }

    public void setIsValid(byte isValid) {
        this.isValid = isValid;
    }
}
