package com.css.chat.logic.chat;

import com.css.chat.base.SessionManager;
import com.css.chat.base.UiBaseService;
import com.css.chat.logic.chat.message.req.ImgChatToUser;
import com.css.chat.logic.chat.message.req.ReqChatToUser;
import com.css.chat.logic.user.UserManager;
import com.css.chat.ui.R;
import com.css.chat.ui.StageController;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ChatManager {

	private static ChatManager self = new ChatManager();

	private ChatManager() {}

	public static ChatManager getInstance() {
		return self;
	}

	/**
	 * 发送文字消息
	 * @author zyj
	 * @date 2019/8/1 15:59
	 * @param friendId
	 * @param content
	 * @return
	 */
	public void sendMessageTo(long friendId, String content) {
		ReqChatToUser request = new ReqChatToUser();
		request.setToUserId(friendId);
		request.setContent(content);

		SessionManager.INSTANCE.sendMessage(request);
	}

	/**
	 * 发送图片
	 * @author zyj
	 * @date 2019/8/1 15:59
	 * @param friendId
	 * @return
	 */
	public void sendImgTo(long friendId) {
		ImgChatToUser request = new ImgChatToUser();
		request.setToUserId(friendId);

		SessionManager.INSTANCE.sendMessage(request);
	}

	public void receiveFriendPrivateMessage(long sourceId, String content) {
		StageController stageController = UiBaseService.INSTANCE.getStageController();
		Stage stage = stageController.getStageBy(R.id.ChatToPoint);
		VBox msgContainer = (VBox)stage.getScene().getRoot().lookup("#msgContainer");

		UiBaseService.INSTANCE.runTaskInFxThread(()-> {
			Pane pane = null;
			if (sourceId != UserManager.getInstance().getMyUserId()) {
				pane = stageController.load(R.layout.PrivateChatItemRight, Pane.class);
			}else {
				pane = stageController.load(R.layout.PrivateChatItemLeft, Pane.class);
			}

			decorateChatRecord(content, pane);
			msgContainer.getChildren().add(pane);
		});

	}

	private void decorateChatRecord(String message, Pane chatRecord) {
		Hyperlink _nikename = (Hyperlink) chatRecord.lookup("#nameUi");
		_nikename.setText(message);
		_nikename.setVisible(false);
		Label _createTime = (Label) chatRecord.lookup("#timeUi");
		_createTime.setText(new SimpleDateFormat("yyyy年MM月dd日  HH:mm:ss").format(new Date()));
		Label _body = (Label) chatRecord.lookup("#contentUi");
		_body.setText(message);
	}

}
