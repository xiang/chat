package com.css.chat.logic.chat;

import com.css.chat.base.SessionManager;
import com.css.chat.logic.chat.message.req.ImgChatToUser;
import com.css.chat.logic.chat.message.req.VideoToUser;

import java.io.File;

/**
 * @description: 发送视频图片
 * @author: zyj
 * @create: 2020-03-04 15:27
 **/
public class SendVideoImg {

    private static SendVideoImg self = new SendVideoImg();

    private SendVideoImg() {}

    public static SendVideoImg getInstance() {
        return self;
    }

    /**
     * 发送图片
     * @author zyj
     * @date 2019/8/1 15:59
     * @param friendId
     * @return
     */
    public void sendVideoImgTo(long friendId, byte[] file, int close) {
        VideoToUser request = VideoToUser.getInstance();
        request.setToUserId(friendId);
        request.setClose(close);
        request.setVideoImg(file);

        SessionManager.INSTANCE.sendVideoImg(request);
    }

}
