package com.css.chat.util;

import org.bytedeco.javacv.CanvasFrame;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author zyj
 * @description
 * @date 2019/7/22 15:33
 **/
public class CommonUtil {
    public static Map<String, CanvasFrame> countCav = new HashMap();

    public static boolean isInteger(String input){
        Matcher mer = Pattern.compile("^[+-]?[0-9]+$").matcher(input);
        return mer.find();
    }

    public static CanvasFrame showCanvas(String fromUserId){
        Iterator<Map.Entry<String, CanvasFrame>> cavIte = countCav.entrySet().iterator();
        while (cavIte.hasNext()){
            Map.Entry<String, CanvasFrame> entry =  cavIte.next();
            String key = entry.getKey();
            if(key.equals(fromUserId)){
                return entry.getValue();
            }
        }
        CanvasFrame canvas = new CanvasFrame("摄像头:"+fromUserId);//新建一个窗口
        canvas.setAlwaysOnTop(true);
        // 获取canvas
        canvas.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                countCav.remove(fromUserId);
            }
        });
        countCav.put(fromUserId, canvas);
        return canvas;
    }



}
