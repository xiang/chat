package com.css.chat.netty.message;

import io.netty.buffer.ByteBuf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.*;

public abstract class AbstractPacket extends ByteBufBean {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void writeBody(ByteBuf buf) {
	}

	@Override
	public void readBody(ByteBuf buf) {
	}

	abstract public PacketType getPacketType();

	abstract public void execPacket();


	/**
	 *  是否开启gzip压缩(默认关闭)
	 *  消息体数据大的时候才开启，非常小的包压缩后体积反而变大，而且耗时
	 */
	public boolean isUseCompression() {
		return false;
	}

	/**
	* @Description: 文件转化成base64字符串
	* @Param: [imgFile]
	* @return: java.lang.String
	* @Author: zyj
	* @Date: 2020/2/27
	*/
	public String fileToBase64Str(File imgFile) {//将图片文件转化为字节数组字符串，并对其进行Base64编码处理

		InputStream in = null;
		byte[] data = null;
		//读取图片字节数组
		try {
			in = new FileInputStream(imgFile);
			data = new byte[in.available()];
			in.read(data);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		//对字节数组Base64编码
		BASE64Encoder encoder = new BASE64Encoder();
		//返回Base64编码过的字节数组字符串
		return encoder.encode(data);
	}


	/**
	* @Description: base64字符串转化成图片
	* @Param: [base64, fileName]
	* @return: java.io.File
	* @Author: zyj
	* @Date: 2020/2/27
	*/
	public static InputStream base64ToInputStream(String base64) {

		BASE64Decoder decoder = new BASE64Decoder();
		byte[] bytes = new byte[1024];

		try {
			bytes = decoder.decodeBuffer(base64);
		} catch (IOException e) {
			e.printStackTrace();
		}

		ByteArrayInputStream stream =  new ByteArrayInputStream(bytes);
		return stream;

	}

	/**
	* @Description: 写文件
	* @Param: [buf, fileUpload]
	* @return: void
	* @Author: zyj
	* @Date: 2020/3/2
	*/
	protected  void writeFile(ByteBuf buf, FileUpload fileUpload){

		try {
			byte[] fileName = fileUpload.getFileName().getBytes("UTF-8");
			buf.writeInt(fileName.length);
			buf.writeBytes(fileName);

			byte[] fileType = fileUpload.getFileType().getBytes("UTF-8");
			buf.writeInt(fileType.length);
			buf.writeBytes(fileType);

			byte[] count = fileUpload.getBytes();
			buf.writeInt(count.length);
			buf.writeBytes(count);

			byte[] base64Count = fileUpload.getBase64Str().getBytes("UTF-8");
			buf.writeInt(base64Count.length);
			buf.writeBytes(base64Count);
			logger.debug("文件数据编码成功");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			logger.debug("文件数据编码失败");
		}
	}

	/**
	* @Description:  读取文件
	* @Param: [buf]
	* @return: com.css.chat.netty.message.FileUpload
	* @Author: zyj
	* @Date: 2020/3/2
	*/
	public FileUpload readFile(ByteBuf buf){

		try {

			int fileNameInt = buf.readInt();
			byte[] fileNameByte = new byte[fileNameInt];
			buf.readBytes(fileNameByte);
			String fileNameStr = new String(fileNameByte,"UTF-8");

			int fileTypeInt = buf.readInt();
			byte[] fileTypeByte = new byte[fileTypeInt];
			buf.readBytes(fileTypeByte);
			String fileTypeStr = new String(fileTypeByte,"UTF-8");

			int countByteInt = buf.readInt();
			byte[] countByte = new byte[countByteInt];
			buf.readBytes(countByte);

			int base64Int = buf.readInt();
			byte[] base64Byte = new byte[base64Int];
			buf.readBytes(base64Byte);
			String base64Str = new String(base64Byte,"UTF-8");

			FileUpload fileUpload = new FileUpload();
			fileUpload.setBytes(countByte);
			fileUpload.setFileName(fileNameStr);
			fileUpload.setFileType(fileTypeStr);
			fileUpload.setBase64Str(base64Str);

			return fileUpload;
		}catch (Exception e){
			logger.error("读取文件失败",e);
		}

		return null;

	}

}
