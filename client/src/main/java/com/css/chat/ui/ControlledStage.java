package com.css.chat.ui;

import javafx.stage.Stage;

public interface ControlledStage {

	Stage getMyStage();

}
